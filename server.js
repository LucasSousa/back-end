const app = require('./app');

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Aplicação exemplo de back-end do workshop rodando na porta ${port}...`));