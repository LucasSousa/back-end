const hello = require('../helpers/hello');

describe('Helper hello', () => {
    it('quando executado com parametro $name deve retornar "Hello $name!" ', () => {
        const name = `jest`;
        expect(hello(name)).toEqual(`Hello ${name}!`);
    });
});