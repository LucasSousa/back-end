const express = require('serverless-express/express');
const app = express();
const hello = require('./helpers/hello');
require('dotenv').config()

app.get('/hello(/:name)?', (req, res) => {
    const name = req.params.name || 'RS/XP';
    res.send({
        message: hello(name),
        dbUser: process.env.DB_USER || "hardCodedDatabaseUser",
        dbPassword: process.env.DB_PASSWORD || "hardCodedDatabasePassword",
    });
});

module.exports = app;